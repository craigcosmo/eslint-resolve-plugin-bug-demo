import React from 'react'
import './home.scss'
import classnames from 'classnames'
export default class UserButton extends React.Component{
	constructor(){
		super()
		this.buttonClicked =false
	}
	userClick(){
		console.log('clicked')
		if (this.buttonClicked ===false) {
			this.buttonClicked = true
			this.props.showLoader()
			this.props.increaseIndex()
			this.props.delayMessage()
		}
	}
	render(){ 
		let style = classnames({
			right: this.buttonClicked,
			center: !this.buttonClicked,
			sep: this.buttonClicked || !this.buttonClicked
		})
		return(
			<div styleName={style}>
				<p onClick={this.userClick.bind(this)} styleName="message user-message">{this.props.item.text}</p>
			</div>
		)
	}
}
UserButton.propTypes ={
	delayMessage: React.PropTypes.func.isRequired,
	increaseIndex: React.PropTypes.func.isRequired,
	showLoader: React.PropTypes.func.isRequired
}