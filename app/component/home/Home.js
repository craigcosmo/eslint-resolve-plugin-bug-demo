import React from 'react'
import config from 'config'
import './home.scss'
import data from 'data/messages.json'
import Loader from 'Loader'
import classnames from 'classnames'
import UserButton from 'UserButton'
import ProgressArc from 'progress-arc-component'
import styled from 'styled-components'


export default class Home extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		
		return (
			<div styleName="home">
				hello world
			</div>
		)
	}
}
