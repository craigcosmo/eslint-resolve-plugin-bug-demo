import React from 'react'
import './loader.scss'


const Loader = () => {
	return (
		<div styleName="loader">
			<div styleName="la-ball-pulse la-sm loading-animation" >
				<div></div>
				<div></div>
				<div></div>
			</div>
		</div>
	)
}

export default Loader