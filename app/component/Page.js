import React from 'react'

import '../style/normalize.css'
import '../style/flexboxgrid.css'
import '../style/page.css'

export default class Page extends React.Component {
	render() {
		return (
			<div className="page">
				{React.cloneElement(this.props.children, {...this.props})}
			</div>
		)
	}
}

