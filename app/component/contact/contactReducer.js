
const defaultState= {
	isPinging: false
}

export default (state = defaultState, action) => {
	switch (action.type) {
		case 'PING':
		return { isPinging: true }

		case 'PONG':
		return { isPinging: false }

		case 'SASA':
		return {...state, masa: action.payload}

		default:
		return state
	}
}