import React from 'react'

import config from 'config'



export default class Home extends React.Component {
	constructor(props) {
		super(props)
	}
	render() {
		return (
			<div>
				<div>
					<h1>is pinging: {this.props.home.isPinging.toString()}</h1>
					<button onClick={this.props.ping.bind(this)}>Start PING</button>
					<button onClick={this.props.strong.bind(this)}>lemon</button>
				</div>
			</div>
		)
	}
}
