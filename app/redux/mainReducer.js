import {combineReducers} from 'redux'
import {routerReducer} from 'react-router-redux'
import {intlReducer} from 'react-intl-redux'
import { combineEpics } from 'redux-observable'
import pingEpic from 'homeEpic'
import homeReducer from 'homeReducer'


export const mainEpic = combineEpics(
	pingEpic
)
// console.log('main',mainEpic)
// console.log('pingepc', pingEpic)
export default combineReducers({
	home:homeReducer,
	routing: routerReducer,
	intl: intlReducer
})

export const mapStateToProps = state => {
	return {
		home: state.home
	}
}