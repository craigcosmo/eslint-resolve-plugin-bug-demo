// process.env.NODE_ENV var is available during build process
const env = process.env.NODE_ENV

let port = 1117
let hostPort = 1118
let url = 'http://localhost:'+port+'/'
let api = 'http://localhost:'+hostPort+'/'



if (env=== 'production') {

	url = 'http://udopy.com/'
	api = 'http://udopy.com/'
}

export const siteUrl = url
export const sitePort = port
export default {
	siteUrl : url,
	img: url+'image/',
	
}