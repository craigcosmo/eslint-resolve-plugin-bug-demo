let webpack = require('webpack')
let	path = require('path')
let dir = require('webpack-directory-scan')
let myModules = dir.get('./','dist')

module.exports = function (config) {
	config.set({
		basePath: '',
		frameworks: ['jasmine'],
		files: [
			'./spec/*.spec.js',
			'./node_modules/es6-promise/dist/es6-promise.js' // this fixes 'Can't find variable: Promise'
		],
		preprocessors: {
			'./spec/*.spec.js': ['webpack']
		},
		webpack: {
			module: {
				loaders: [
				{
					test: /\.(js|jsx)$/,
					loader: 'babel-loader',
					exclude: /node_modules/,
					query: {
						presets: ['es2015', 'react']
					}
				},
				{
					test: /\.(css|scss)$/,
					loader: 'null-loader'
				},
				{
					test: /\.(woff|woff2|eot|ttf)$/i,
					loader: 'null-loader'
				},
				{
					test: /\.(jpe?g|gif|png|svg)$/i,
					loader: 'null-loader'
				},
				{
					test: /vendor\/.+\.(jsx|js)$/,
					loader: 'imports?jQuery=jquery,$=jquery,this=>window'
				}
				]
			},
			resolve: {
				modules: [...myModules, 'node_modules'],
			},
			externals: {
				'jsdom': 'window',
				'cheerio': 'window',
				'react/addons': true,
				'react/lib/ExecutionEnvironment': true,
				'react/lib/ReactContext': true
			},
			node: {
				fs: 'empty' // this fixes 'can not resolve module fs'
			}
		},
		webpackMiddleware: {
			noInfo: true
		},
		plugins: [
			'karma-webpack',
			'karma-jasmine',
			'karma-phantomjs-launcher',
			'karma-chrome-launcher',
			'karma-verbose-reporter',
		],
		reporters: ['verbose','progress'],
		port: 9876,
		colors: true,
		logLevel: config.LOG_INFO,
		autoWatch: true,
		browsers: ['Chrome'],
		singleRun: false
	})
}
